#include <mysql/mysql.h>
#include <string.h>
#include <stdlib.h>
#include "table_to_struct.h"

static MYSQL mysql;
static MYSQL_RES * res;
static int line;
static int row;

static int table_count;
static int table_fields;
static MYSQL_ROW table_row;

MonsterTemplate * g_monster_template;

void connect_database()
{
	    mysql_init(&mysql);
        mysql_real_connect(&mysql,"localhost","root","123456","zhtx_template",0,NULL,0);
}

int t_monster_template()
{

	mysql_query(&mysql,"select * from dbtablename;");
	res = mysql_use_result(&mysql);
	table_count = mysql_field_count(&mysql);

	g_monster_template = (MonsterTemplate *)malloc(sizeof(MonsterTemplate)*table_count);

    for(line=0;line<=table_count;line++)
         {
            table_row = mysql_fetch_row(res);
      
            g_monster_template[line].monster_id = atoi(table_row[0]);
			g_monster_template[line].name=(char *)malloc(sizeof(table_row[1]));
			memcpy(g_monster_template[line].name , table_row[1],sizeof(table_row[1]));
			g_monster_template[line].element_type = atoi(table_row[2]);
			g_monster_template[line].level = atoi(table_row[3]);
			g_monster_template[line].quality = atoi(table_row[4]);
			g_monster_template[line].monster_type = atoi(table_row[5]);
			g_monster_template[line].affiliate_type = atoi(table_row[6]);
			g_monster_template[line].max_hp = atoi(table_row[7]);
			g_monster_template[line].hp_recover = atoi(table_row[8]);
			g_monster_template[line].camp = atoi(table_row[9]);
			g_monster_template[line].act_type = atoi(table_row[10]);
			g_monster_template[line].attack_physics = atoi(table_row[11]);
			g_monster_template[line].attack_range = atoi(table_row[12]);
			g_monster_template[line].attack_magic = atoi(table_row[13]);
			g_monster_template[line].attack_vindictive = atoi(table_row[14]);
			g_monster_template[line].attack_speed = atoi(table_row[15]);
			g_monster_template[line].defanse_physics = atoi(table_row[16]);
			g_monster_template[line].defanse_range = atoi(table_row[17]);
			g_monster_template[line].defanse_magic = atoi(table_row[18]);
			g_monster_template[line].defanse_vindictive = atoi(table_row[19]);
			g_monster_template[line].attack_burn = atoi(table_row[20]);
			g_monster_template[line].attack_faint = atoi(table_row[21]);
			g_monster_template[line].attack_poison = atoi(table_row[22]);
			g_monster_template[line].attack_ice = atoi(table_row[23]);
			g_monster_template[line].burn_counteractant = atoi(table_row[24]);
			g_monster_template[line].faint_counteractant = atoi(table_row[25]);
			g_monster_template[line].poison_counteractant = atoi(table_row[26]);
			g_monster_template[line].ice_counteractant = atoi(table_row[27]);
			g_monster_template[line].hit = atoi(table_row[28]);
			g_monster_template[line].dodge = atoi(table_row[29]);
			g_monster_template[line].critical = atoi(table_row[30]);
			g_monster_template[line].tough = atoi(table_row[31]);
			g_monster_template[line].anti_effect = atoi(table_row[32]);
			g_monster_template[line].exp_fixed = atoi(table_row[33]);
			g_monster_template[line].exp = atoi(table_row[34]);
			g_monster_template[line].move_speed = atoi(table_row[35]);
			g_monster_template[line].act_distince = atoi(table_row[36]);
			g_monster_template[line].return_distince = atoi(table_row[37]);
			g_monster_template[line].guard_distince = atoi(table_row[38]);
			g_monster_template[line].reborn_time = atoi(table_row[39]);
			g_monster_template[line].reborn_animation = atoi(table_row[40]);
			g_monster_template[line].die_time = atoi(table_row[41]);
			g_monster_template[line].pic_path=(char *)malloc(sizeof(table_row[42]));
			memcpy(g_monster_template[line].pic_path , table_row[42],sizeof(table_row[42]));
			g_monster_template[line].map_id = atoi(table_row[43]);
			g_monster_template[line].local_point=(char *)malloc(sizeof(table_row[44]));
			memcpy(g_monster_template[line].local_point , table_row[44],sizeof(table_row[44]));
			g_monster_template[line].lbornl_points=(char *)malloc(sizeof(table_row[45]));
			memcpy(g_monster_template[line].lbornl_points , table_row[45],sizeof(table_row[45]));
			g_monster_template[line].description=(char *)malloc(sizeof(table_row[46]));
			memcpy(g_monster_template[line].description , table_row[46],sizeof(table_row[46]));
			g_monster_template[line].can_attacked = atoi(table_row[47]);
			g_monster_template[line].is_boss = atoi(table_row[48]);
			g_monster_template[line].show_type = atoi(table_row[49]);
			g_monster_template[line].jump_data=(char *)malloc(sizeof(table_row[50]));
			memcpy(g_monster_template[line].jump_data , table_row[50],sizeof(table_row[50]));
			g_monster_template[line].scale = atoi(table_row[51]);
			g_monster_template[line].common_drop=(char *)malloc(sizeof(table_row[52]));
			memcpy(g_monster_template[line].common_drop , table_row[52],sizeof(table_row[52]));
			g_monster_template[line].mutex_drop=(char *)malloc(sizeof(table_row[53]));
			memcpy(g_monster_template[line].mutex_drop , table_row[53],sizeof(table_row[53]));
			g_monster_template[line].mutex_percent=(char *)malloc(sizeof(table_row[54]));
			memcpy(g_monster_template[line].mutex_percent , table_row[54],sizeof(table_row[54]));
			g_monster_template[line].grou_up=(char *)malloc(sizeof(table_row[55]));
			memcpy(g_monster_template[line].grou_up , table_row[55],sizeof(table_row[55]));
			g_monster_template[line].skill=(char *)malloc(sizeof(table_row[56]));
			memcpy(g_monster_template[line].skill , table_row[56],sizeof(table_row[56]));
			g_monster_template[line].hp_skill=(char *)malloc(sizeof(table_row[57]));
			memcpy(g_monster_template[line].hp_skill , table_row[57],sizeof(table_row[57]));
			g_monster_template[line].timer_skill=(char *)malloc(sizeof(table_row[58]));
			memcpy(g_monster_template[line].timer_skill , table_row[58],sizeof(table_row[58]));
			g_monster_template[line].hp_event=(char *)malloc(sizeof(table_row[59]));
			memcpy(g_monster_template[line].hp_event , table_row[59],sizeof(table_row[59]));
			g_monster_template[line].time_event=(char *)malloc(sizeof(table_row[60]));
			memcpy(g_monster_template[line].time_event , table_row[60],sizeof(table_row[60]));
			g_monster_template[line].other_info=(char *)malloc(sizeof(table_row[61]));
			memcpy(g_monster_template[line].other_info , table_row[61],sizeof(table_row[61]));
			g_monster_template[line].other_data=(char *)malloc(sizeof(table_row[62]));
			memcpy(g_monster_template[line].other_data , table_row[62],sizeof(table_row[62]));
 
         }

    return 0;

}

int load_all_table()
{
	connect_database();
	t_monster_template();
	return 0;

}
