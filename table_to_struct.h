﻿#ifndef __TABLE_TO_STRUCT_H__
#define __TABLE_TO_STRUCT_H__ 
#include "common_struct.h"

//怪物模板表
typedef struct
  {
		int monster_id;
		char *name
		int element_type;
		int level;
		int quality;
		int monster_type;
		int affiliate_type;
		int max_hp;
		int hp_recover;
		int camp;
		int act_type;
		int attack_physics;
		int attack_range;
		int attack_magic;
		int attack_vindictive;
		int attack_speed;
		int defanse_physics;
		int defanse_range;
		int defanse_magic;
		int defanse_vindictive;
		int attack_burn;
		int attack_faint;
		int attack_poison;
		int attack_ice;
		int burn_counteractant;
		int faint_counteractant;
		int poison_counteractant;
		int ice_counteractant;
		int hit;
		int dodge;
		int critical;
		int tough;
		int anti_effect;
		int exp_fixed;
		int exp;
		int move_speed;
		int act_distince;
		int return_distince;
		int guard_distince;
		int reborn_time;
		int reborn_animation;
		int die_time;
		char *pic_path
		int map_id;
		char *local_point
		char *lbornl_points
		char *description
		int can_attacked;
		int is_boss;
		int show_type;
		char *jump_data
		int scale;
		char *common_drop
		char *mutex_drop
		char *mutex_percent
		char *grou_up
		char *skill
		char *hp_skill
		char *timer_skill
		char *hp_event
		char *time_event
		char *other_info
		char *other_data

}MonsterTemplate;



#endif