﻿#ifndef __COMMON_STRUCT_H__
#define __COMMON_STRUCT_H__

typedef struct
  {
    int pos_x;
    int pos_y;  
  }Pos;

typedef struct
{
      int is_frozen;                      //是否被冰冻
      int is_faint;                       //是否被眩晕
      int is_stop_move;                   //是否被定身
      int is_slow_down;                    //是否被减速
}IsDebuff;

typedef struct
  {
      int tmp_attack_physics;             //普攻
      int tmp_attack_vindictive;          //火攻
      int tmp_attack_magic;               //雷攻
      int tmp_attack_range;               //风攻
      int tmp_defanse_physics;            //普防
      int tmp_defanse_vindictive;         //火防
      int tmp_defanse_magic;              //雷防
      int tmp_defanse_range;              //风防
      int tmp_hit;                        //命中
      int tmp_dodge;                      //闪避
      int tmp_critical;                   //暴击
      int tmp_tough;                      //坚韧
      int tmp_anti_effect;                //抗性
      int tmp_burn_attack;                //燃烧
      int tmp_burn_counteractant;         //燃烧抗性
      int tmp_faint_attack;               //眩晕攻击
      int tmp_faint_counteractant;        //眩晕抗性
      int tmp_poison_attack;              //毒性攻击
      int tmp_poison_counteractant;       //毒性抗性
      int tmp_ice_attack;                 //冰封攻击
      int tmp_ice_counteractant;          //冰封抗性
      int tmp_move_speed;                 //移动速度
      int tmp_avoid;                      //免伤
      int tmp_strong;                     //
}TmpAttributes;

typedef struct
{
      int attack_physics;                     // 普通攻击力      
      int attack_range;                       // 远程伤害 
      int attack_magic;                       // 魔法伤害 
      int attack_vindictive;                  // 斗气伤害 
      int attack_speed;                       // 攻击速度 
      int defanse_physics;                    // 普通防御力      
      int defanse_range;                      // 远程防御 
      int defanse_magic;                      // 魔法防御 
      int defanse_vindictive;                 // 斗气防御
      int attack_burn;                        // 燃烧攻击
      int attack_faint;                       // 眩晕攻击
      int attack_poison;                      // 毒性攻击
      int attack_ice;                         // 冰封攻击 
      int burn_counteractant;                 // 燃烧抗性
      int faint_counteractant;                // 眩晕抗性
      int poison_counteractant;               // 毒性抗性
      int ice_counteractant;                  // 冰封抗性
      int hit;                                // 命中   
      int dodge;                              // 闪避   
      int critical;                           // 暴击   
      int tough;                              // 坚韧   
      int anti_effect;                        // 抗性
      int move_speed;                         // 移动速度 
}Attributes;


typedef struct
  {
  }MonsterSkill;

  typedef struct
  {
  }HpEvent;

  typedef struct
  {
  }TimeEvent;

  typedef struct
  {
  }DieEvent;

  typedef struct
  {
  }SyleBin;
  
  typedef struct
  {
  }BuffInfo;

  typedef struct
  {
  }Path;

  typedef struct
  {
  }HartedList;

  #endif