objects = data_base_load.o unit_testing.o
test:clear $(objects)
	gcc $(objects) -o test -L /usr/local/mysql/lib/ -l mysqlclient

data_base_load.o:
	gcc -c data_base_load.c -L /usr/local/mysql/lib/ -l mysqlclient
	
unit_testing.o:

clear:
	rm -rf *.o test
	